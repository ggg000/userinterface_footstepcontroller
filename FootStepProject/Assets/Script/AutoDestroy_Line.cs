﻿using UnityEngine;
using System.Collections;

/// <summary>
/// LineRendererを自動的に消滅させる
/// 音が鳴った時に表示されるオレンジの棒を制御
/// </summary>
public class AutoDestroy_Line : MonoBehaviour
{
    [SerializeField]
    private float mDestroyTime = 1f;
    private float mInitialTime = 0f;
    private LineRenderer mLineRenderer;

    void Start()
    {
        mInitialTime = Time.time;
        mLineRenderer = this.gameObject.GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        float time = Time.time - mInitialTime;
        if (time > mDestroyTime)
        {
            GameObject.Destroy(this.gameObject);
            return;
        }

        //時間に応じて透明化
        Color c = mLineRenderer.material.color;
        c.a = 1f - (time / mDestroyTime);
        mLineRenderer.material.color = c;

    }
}
