﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// Unityの使用上、スクロールがなぜ0.5(真ん中)で初期化される
/// このクラスを使って無理やり0に初期化
/// </summary>
public class ResetScroll : MonoBehaviour
{
    private Scrollbar mScroll;
    public float mResetVal = 0f;

    void Start()
    {
        mScroll = this.gameObject.GetComponent<Scrollbar>();
        mScroll.value = mResetVal;
    }

}
