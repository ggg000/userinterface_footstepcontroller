﻿using UnityEngine;
using System.Collections;

/// <summary>
/// FFTを使って周波数特徴を求める
/// 現在未使用
/// </summary>
public class FrequencyLogger : MonoBehaviour
{
    private const int mMaxCount = 64; //power of 2
    private double[] mDataX = new double[mMaxCount];
    private double[] mSpectrumX = new double[mMaxCount];
    private int mCurrentCount = 0;
    private bool mInitial = true;
    private double[] mDummy = new double[mMaxCount];
#pragma warning disable 414
    private double[] mDummy2 = new double[mMaxCount];
#pragma warning restore 414

    private double mLowX = 0f;
    private double mMidX = 0f;
    private double mHighX = 0f;
    private float mF = 0f;


    public float GetF()
    {
        return mF;
    }

    public void FastUpdate()
    {
        mLowX = 0; mMidX = 0; mHighX = 0;

        Vector3 ac = Input.acceleration;
        mDataX[mCurrentCount] = (ac.x + 1.01f) / 2.02f;

        mCurrentCount = (mCurrentCount + 1) % mMaxCount;

        if (mInitial) mInitial = (mCurrentCount == (mMaxCount - 1));
        else
        {
            FFT(mDataX, mDummy, out mSpectrumX, out mDummy2, 6);


            for (int i = 0; i < mMaxCount; ++i)
            {
                mSpectrumX[i] = (mSpectrumX[i] < 0.0001f) ? 0f : mSpectrumX[i] * 1000f;

                if (i == 0) mLowX += mSpectrumX[i];
                else if (i > (mMaxCount * 1) / 2) mHighX += mSpectrumX[i];
                else mMidX += mSpectrumX[i];
            }

            mF = (float)(mHighX + mMidX);
        }
    }

    public static void FFT(
            double[] inputRe,
            double[] inputIm,
            out double[] outputRe,
            out double[] outputIm,
            int bitSize)
    {
        int dataSize = 1 << bitSize;
        int[] reverseBitArray = BitScrollArray(dataSize);

        outputRe = new double[dataSize];
        outputIm = new double[dataSize];

        // バタフライ演算のための置き換え
        for (int i = 0; i < dataSize; i++)
        {
            outputRe[i] = inputRe[reverseBitArray[i]];
            outputIm[i] = inputIm[reverseBitArray[i]];
        }

        // バタフライ演算
        for (int stage = 1; stage <= bitSize; stage++)
        {
            int butterflyDistance = 1 << stage;
            int numType = butterflyDistance >> 1;
            int butterflySize = butterflyDistance >> 1;

            double wRe = 1.0;
            double wIm = 0.0;
            double uRe =
                System.Math.Cos(System.Math.PI / butterflySize);
            double uIm =
                -System.Math.Sin(System.Math.PI / butterflySize);

            for (int type = 0; type < numType; type++)
            {
                for (int j = type; j < dataSize; j += butterflyDistance)
                {
                    int jp = j + butterflySize;
                    double tempRe =
                        outputRe[jp] * wRe - outputIm[jp] * wIm;
                    double tempIm =
                        outputRe[jp] * wIm + outputIm[jp] * wRe;
                    outputRe[jp] = outputRe[j] - tempRe;
                    outputIm[jp] = outputIm[j] - tempIm;
                    outputRe[j] += tempRe;
                    outputIm[j] += tempIm;
                }
                double tempWRe = wRe * uRe - wIm * uIm;
                double tempWIm = wRe * uIm + wIm * uRe;
                wRe = tempWRe;
                wIm = tempWIm;
            }
        }
    }

    // ビットを左右反転した配列を返す
    private static int[] BitScrollArray(int arraySize)
    {
        int[] reBitArray = new int[arraySize];
        int arraySizeHarf = arraySize >> 1;

        reBitArray[0] = 0;
        for (int i = 1; i < arraySize; i <<= 1)
        {
            for (int j = 0; j < i; j++)
                reBitArray[j + i] = reBitArray[j] + arraySizeHarf;
            arraySizeHarf >>= 1;
        }
        return reBitArray;
    }
}
