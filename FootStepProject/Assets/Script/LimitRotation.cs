﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Unityの使用上、向きを変えた時に勝手にUIが回転する
/// 全部の回転を禁止するための処理を実装
/// </summary>
public class LimitRotation : MonoBehaviour
{

    void Update()
    {
        switch (Screen.orientation)
        {
            case ScreenOrientation.Portrait:
                Screen.orientation = ScreenOrientation.Portrait;
                break;
            case ScreenOrientation.PortraitUpsideDown:
                Screen.orientation = ScreenOrientation.PortraitUpsideDown;
                break;
            case ScreenOrientation.LandscapeLeft:
                Screen.orientation = ScreenOrientation.LandscapeLeft;
                break;
            case ScreenOrientation.LandscapeRight:
                Screen.orientation = ScreenOrientation.LandscapeRight;
                break;
        }
    }
}
