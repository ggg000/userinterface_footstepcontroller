﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// 上下にスクロールすると消えたりあらわれたりする矢印の制御
/// mTopで上に行ったときにけるか下に行ったときに消えるかを切り替え
/// </summary>
public class ScrollArrow : MonoBehaviour {
    [SerializeField]
    private Scrollbar mScroll;
    private Image mImage;
    public bool mTop;

	void Start () {
        mImage = gameObject.GetComponent<Image>();
        if (mTop) mImage.CrossFadeAlpha(0.0f, 0.0f, false);
	}
	
	void Update () {
        if (!mTop)
        {
            if (mScroll.value >= 1.0f) mImage.CrossFadeAlpha(0.0f, 0.1f, false);
            else mImage.CrossFadeAlpha(0.65f, 0.05f, false);
        }
        else
        {
            if (mScroll.value <= 0.0f) mImage.CrossFadeAlpha(0.0f, 0.1f, false);
            else mImage.CrossFadeAlpha(0.65f, 0.05f, false);
        }
	}
}
