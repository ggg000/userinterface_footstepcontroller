﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// 加速度情報を元に音を鳴らすかどうかを判定する
/// </summary>
public class WaveLogger : MonoBehaviour
{
    #region PUBLIC FIELD
    [SerializeField]
    private float mLeftLimit = -10f;
    [SerializeField]
    private float mRightLimit = 10f;
    [SerializeField]
    private int mMaxCount = 1000;
    [SerializeField]
    private float mFPS = 100f;
    [SerializeField]
    private float mMaximumFPS = 500f;
    [SerializeField]
    private LineRenderer mLineForX;
    [SerializeField]
    private LineRenderer mLineForY;
    [SerializeField]
    private LineRenderer mLineForZ;
    [SerializeField]
    private LineRenderer mLineForAll;
    [SerializeField]
    private LineRenderer mLineOfTime;
    [SerializeField]
    private float mLengthOfLineOfTime = 3.5f;
    [SerializeField]
    private LineRenderer mLineOfHit;

	[SerializeField]
	private float mLowPathScale = 2.0f;
    [SerializeField]
    private Canvas mSelectSoundUI;
    [SerializeField]
    private Canvas mPlayWaveUI;
    [SerializeField]
    private GameObject mDebugCircle;
    #endregion

    #region PRIVATE FIELD
    private AudioController mAudioCtr;

    private bool mIsThisFirstUpdate = true;
    private int mFirstStopCount = 200;
	
    private Vector3 mLowPathFilter = Vector3.zero;
	private Vector3 mLerpFilter = Vector3.zero;

    private int mCurrentVertexID = 0;

    private float mSPF;
    private float mPreFpsSetTime = 0f;
    private float mPreUpdateTime = 0f;
    private int mFrame = 0;
    private bool mStopUpdate = false;

    private float mMinUpdatePeriod;
	private float mMaxUpdatePeriod;

#pragma warning disable 414
    private float mCurrentFPS = 0f;
#pragma warning restore 414

    private float mPreStepTime = -10f;
    private float mMinStepPeriod = 0.5f;

	//計算を行うフレーム数
	private const int CalculusFrame = 50;//微分するフレーム数
	private const int DeltaXFrame = 18;//x軸変化量用フレーム数
	private const int SignedFrame = 10;//符号付き変化量を計算するフレーム数
	private const int PreFrame = 5;//予測するフレーム数

	//x軸変化量用パラメータ
	private int framecounterX = 0;
	private float[] ValueX = new float[DeltaXFrame];
	private float DeltaX = 0f;
	private float DeltaXLerp = 0f;

    //時間経過計算用パラメータ
    private float lastMainFootStepTime = 0f;
    private bool firstCalibration = true;
	private bool firstSetting = true;
    private int currentCountOfFootStepTimes = 0;
    private float[] footStepTimes = new float[3];
    private float TimeLerp = 0f;

	//キャップ用パラメータ
	private float Cap = 0f;
	private float capLerp = 0f;


	//予測用パラメータ
	private int signedcounter = 0;
	private float PreviousValueSigned = 0f;
	private float DeltaOfFrameSigned = 0f;//符号付き変化量
	private float[] deltaAllSigned = new float[SignedFrame];
	private float RateSigned = 0f;
	private float PreCap = 0f;//予測のキャップ
	private float sumDeltaSigned = 0f;

	//足判定用パラメータ
	private bool MainFlag = false;
    #endregion

    // Use this for initialization
    void Start()
    {
        mLerpFilter = Input.acceleration;
        mLowPathFilter = Input.acceleration;
        mCurrentVertexID = 0;
        mAudioCtr = FindObjectOfType<AudioController>();

        mLineForX.SetVertexCount(mMaxCount);
        mLineForY.SetVertexCount(mMaxCount);
        mLineForZ.SetVertexCount(mMaxCount);
        mLineForAll.SetVertexCount(mMaxCount);
        mLineOfTime.SetVertexCount(2);

		mMinUpdatePeriod = 0.01f;
		mMaxUpdatePeriod = 0.12f;

        //加速度表示(4色のラインとピンクの棒)の初期化
        float xPos;
        for (int i = 0; i < mMaxCount; ++i)
        {
            xPos = (mRightLimit - mLeftLimit) * ((float)i / (float)mMaxCount) + mLeftLimit;

            mLineForX.SetPosition(i, new Vector3(xPos, 0f, 0f));
            mLineForY.SetPosition(i, new Vector3(xPos, 0f, 0f));
            mLineForZ.SetPosition(i, new Vector3(xPos, 0f, 0f));
            mLineForAll.SetPosition(i, new Vector3(xPos, 0f, 0f));
        }
        mLineOfTime.SetPosition(0, new Vector3(mLeftLimit, mLengthOfLineOfTime, 0f));
        mLineOfTime.SetPosition(1, new Vector3(mLeftLimit, -mLengthOfLineOfTime, 0f));

        mSPF = 1f / mFPS; //FPSからフレーム間時間計算
        mPreUpdateTime = -1f * mSPF;
        InvokeRepeating("FastUpdateBase", 0.0001f, 1f / mMaximumFPS);

        //最初は更新しない
        mStopUpdate = true;
    }

    //更新するかどうかを切り替え(Menu画面移行時に使用)
    public void SetUpdateFlag(bool flag)
    {
        mStopUpdate = !flag;
        mSelectSoundUI.gameObject.SetActive(mStopUpdate);
        mPlayWaveUI.gameObject.SetActive(!mStopUpdate);
    }
    
    //高速更新制御
    private void FastUpdateBase()
    {

        float now = Time.realtimeSinceStartup;
        float delta = (now - mPreUpdateTime);


        //高速更新
        while (delta > mSPF)
        {
            mFrame += 1;
            if (!mStopUpdate) FastUpdate();
            mPreUpdateTime += mSPF;

            delta = now - mPreUpdateTime;
        }

        //実FPS計測(テスト用)
        float time = (Time.realtimeSinceStartup - mPreFpsSetTime);
        if (time > 0.1f)
        {
            mCurrentFPS = mFrame / time;
            mFrame = 0;
            mPreFpsSetTime = Time.realtimeSinceStartup;
        }
    }

    //実際の更新関数
    private void FastUpdate()
    {
        #region フィルタリング
        //ローパスフィルタ通過
        mLowPathFilter += 0.1f * (Input.acceleration - mLowPathFilter);

        //変化率拡張フィルタ通過
		float bi = 1f + Mathf.InverseLerp(mMinUpdatePeriod, mMaxUpdatePeriod, (Input.acceleration - mLerpFilter).magnitude) * 2.0f;
        mLerpFilter += 0.5f * (bi * (Input.acceleration- mLerpFilter));
        #endregion

        //加速度のGUI表示
        float xPos = (mRightLimit - mLeftLimit) * ((float)mCurrentVertexID / (float)mMaxCount) + mLeftLimit;
        float zPos = 0f;
        mLineForX.SetPosition(mCurrentVertexID, new Vector3(xPos, mLowPathFilter.x * mLowPathScale, zPos));
        mLineForY.SetPosition(mCurrentVertexID, new Vector3(xPos, mLowPathFilter.y * mLowPathScale, zPos));
        mLineForZ.SetPosition(mCurrentVertexID, new Vector3(xPos, mLowPathFilter.z * mLowPathScale, zPos));
        mLineForAll.SetPosition(mCurrentVertexID, new Vector3(xPos, mLowPathFilter.magnitude * mLowPathScale, zPos));
        mLineOfTime.SetPosition(0, new Vector3(xPos, mLengthOfLineOfTime, 0f));
        mLineOfTime.SetPosition(1, new Vector3(xPos, -mLengthOfLineOfTime, 0f));

        //加速度インデックス更新
        mCurrentVertexID = (mCurrentVertexID + 1) % mMaxCount;

        #region【Delta】:DeltaXFrame分のフレーム毎のx軸の変化量
        ValueX[framecounterX] = mLowPathFilter.x;
		DeltaX = (-1) * (ValueX[framecounterX] - ValueX[(framecounterX + 1) % DeltaXFrame]) * 10000;
		DeltaXLerp = 100 * Mathf.InverseLerp(0, 625, DeltaX); //0から100にスケーリング
		framecounterX = (framecounterX + 1) % DeltaXFrame; //次の計算用
        #endregion

        #region【Time】:時間経過(サブ足用)
        if (MainFlag) //前がメイン足
        {
			if (!firstCalibration)
            {
                float flow = Time.realtimeSinceStartup - lastMainFootStepTime;
                float sum = 0f; 
				foreach (float i in footStepTimes) sum += i; //数歩分の平均計算
                sum /= footStepTimes.Length;                 //デモ時は3歩
                sum /= 2f; //2歩毎計算なので

                TimeLerp = 100 * Mathf.InverseLerp(sum * 0.5f, sum * 1.25f, flow); //0から100にスケーリング
            }
        }
        #endregion

        #region【Cap】:予測キャップ
        //変化量を記録し、その平均を元に数フレーム後の値を予測
		DeltaOfFrameSigned = (mLerpFilter.magnitude - PreviousValueSigned);
		PreviousValueSigned = mLerpFilter.magnitude;
		deltaAllSigned [signedcounter] = DeltaOfFrameSigned;
		sumDeltaSigned = 0f;
		for (int loop = 0; loop < signedcounter; loop++) {
			sumDeltaSigned += deltaAllSigned[loop];
		}
		signedcounter = (signedcounter + 1) % SignedFrame;
        RateSigned = sumDeltaSigned / SignedFrame;
		PreCap = mLerpFilter.magnitude + RateSigned * PreFrame;

        Cap = Mathf.Max(PreCap, mLerpFilter.magnitude); //予測無と予測込の大きいほうを使用
        capLerp = 100 * Mathf.InverseLerp(10000f, 20000f, Cap * 10000); //0から100にスケーリング
        #endregion

        //最初しばらくは慣らさない(変化率が異常に大きくなるため)
        bool footStep = false;
        if (!mIsThisFirstUpdate) footStep = FootStep(); //実際に音を鳴らすかを判定
        else
        {
            mFirstStopCount -= 1;
            if (mFirstStopCount <= 0) mIsThisFirstUpdate = false;
        }

        //足踏みの感覚を計算(Time計算用)
        if (MainFlag && footStep) //左足の条件を満たしている？
        {
            //スマホが入っている足のステップからの経過時間
            if (firstSetting)
            {
                lastMainFootStepTime = Time.realtimeSinceStartup;
				firstSetting=false;
            }
            else
            {
                float time = Time.realtimeSinceStartup - lastMainFootStepTime;
                footStepTimes[currentCountOfFootStepTimes] = time;
                currentCountOfFootStepTimes = (currentCountOfFootStepTimes + 1);
                if (firstCalibration) firstCalibration = !(currentCountOfFootStepTimes == 5);
                currentCountOfFootStepTimes %= footStepTimes.Length;

                lastMainFootStepTime = Time.realtimeSinceStartup;
            }
        }

        //音を鳴らす時のGUI表示
		if (footStep)
		{
			GameObject.Instantiate(mDebugCircle, new Vector3(xPos, mLowPathFilter.x * mLowPathScale, zPos), Quaternion.identity);
			GameObject.Instantiate(mDebugCircle, new Vector3(xPos, mLowPathFilter.y * mLowPathScale, zPos), Quaternion.identity);
			GameObject.Instantiate(mDebugCircle, new Vector3(xPos, mLowPathFilter.z * mLowPathScale, zPos), Quaternion.identity);
			GameObject.Instantiate(mDebugCircle, new Vector3(xPos, mLowPathFilter.magnitude * mLowPathScale, zPos), Quaternion.identity);
            GameObject g = GameObject.Instantiate(mLineOfHit.gameObject, new Vector3(0f, 0f), Quaternion.identity) as GameObject;
            LineRenderer lr = g.GetComponent<LineRenderer>();
            lr.SetPosition(0, new Vector3(xPos, mLengthOfLineOfTime, 0f));
            lr.SetPosition(1, new Vector3(xPos, -mLengthOfLineOfTime, 0f));
		}


    }

    //音を鳴らすかどうかを実際に判定
    private bool FootStep()
    {
		bool flag = false;

		if (Time.realtimeSinceStartup - mPreStepTime > mMinStepPeriod) //最低数ミリ秒の感覚を挟む
        {
            if (MainFlag){ //前がメイン足ならサブ足判定
				if(SubStep()){
					MainFlag = false;
					flag = true;
				}
			}else{ //前がサブ足ならメイン足判定
				if(MainStep()){
					MainFlag = true;
					flag = true;
				}
			}

			if (flag){
                bool step = mAudioCtr.PlayAudio(); //音を鳴らす
				if (!step) return false;
				else
				{
					mPreStepTime = Time.realtimeSinceStartup;
					return true;
				}
			}
        }

        return false;
    }

    //メイン足の接地判定
	private bool MainStep(){
		if(DeltaXLerp * 0.5 + capLerp * 0.5> 60)
			return true;
		return false;
	}

    //サブ足の接地判定
	private bool SubStep(){
		if (!firstCalibration) {
			if(DeltaXLerp * 0.8 + TimeLerp * 0.2 > 60)
				return true;
		} else {
			if(DeltaXLerp * 1 > 60)
				return true;
		}
		return false;
	}
}
