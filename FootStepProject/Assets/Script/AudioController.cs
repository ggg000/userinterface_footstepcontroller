﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 音を鳴らしたり、鳴らす音を管理する
/// </summary>
public class AudioController : MonoBehaviour
{
    [SerializeField]
    private AudioSource[] mAudioSource = new AudioSource[3];

    private int mCurrentUsingAudio = 0;

    //左右両方に同じ音をセット
    public void SetAudio(AudioClip clip)
    {
        for (int i = 0; i < 2; ++i) mAudioSource[i].clip = clip;
    }

    //メイン足に音をセット(未使用)
    public void SetAudio1(AudioClip clip)
    {
        mAudioSource[0].clip = clip;
    }

    //サブ足に音をセット(未使用)
    public void SetAudio2(AudioClip clip)
    {
        mAudioSource[1].clip = clip;
    }

    //音試聴
    public void TryAudio(AudioClip clip)
    {
        mAudioSource[0].PlayOneShot(clip);
    }

    //音停止(メニュー画面移行時用)
    public void StopAudio()
    {
        for (int i = 0; i < 2; ++i) if(mAudioSource[i].isPlaying) mAudioSource[i].Stop();
    }

    //音が鳴ったらtrue、鳴らなかったらfalse
    public bool PlayAudio()
    {
        if (mAudioSource[mCurrentUsingAudio])
        {
            mAudioSource[mCurrentUsingAudio].Play();
            mCurrentUsingAudio = (mCurrentUsingAudio + 1) % 2;
            return true;
        }
        else return false;
    }

}
