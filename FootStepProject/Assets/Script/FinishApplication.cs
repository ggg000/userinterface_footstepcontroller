﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Unityの仕様上、リターンボタンを押しただけでは終了されない
/// リターンボタン時にアプリを落とす処理を実装
/// </summary>
public class FinishApplication : MonoBehaviour
{

    void Update()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            // エスケープキー取得
            if (Input.GetKey(KeyCode.Escape))
            {
                // アプリケーション終了
                Application.Quit();
                return;
            }
        }
    }
}
