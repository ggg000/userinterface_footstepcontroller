﻿using UnityEngine;
using System.Collections;

/// <summary>
/// SpriteRendererを自動的に消滅させる
/// 音が鳴った時に表示されるオレンジの◎を制御
/// </summary>
public class AutoDestroy : MonoBehaviour
{
    [SerializeField]
    private float mDestroyTime = 1f;
    private float mInitialTime = 0f;
    private SpriteRenderer mSpriteRenderer;

    void Start()
    {
        mInitialTime = Time.time;
        mSpriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        float time = Time.time - mInitialTime;
        if (time > mDestroyTime)
        {
            GameObject.Destroy(this.gameObject);
            return;
        }

        //時間に応じて透明化
        Color c = mSpriteRenderer.material.color;
        c.a = 1f - (time / mDestroyTime);
        mSpriteRenderer.material.color = c;

    }
}
